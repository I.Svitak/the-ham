const people = [
  {
    id: 1,
    name: "Abhisek Tamang",
    title: "UX Designer",
    photoUrl: "./img/people/Abhisek_Tamang.jpg",
    text: "Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.",
  },
  {
    id: 2,
    name: "Andrew Kavinsky",
    title: "Project Manager",
    photoUrl: "./img/people/Andrew_Kavinsky.jpg",
    text: "Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis",
  },
  {
    id: 3,
    name: "Max Cavalera",
    title: "React Developer",
    photoUrl: "./img/people/Max_Cavalera.jpg",
    text: "Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.",
  },
  {
    id: 4,
    name: "Woo Chang",
    title: "HR Manager",
    photoUrl: "./img/people/Woo_Chang.jpg",
    text: "Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.",
  },
];

const photoElement = document.querySelector(".section3-photo img");
const textElement = document.querySelector(".section3-text");
const nameElement = document.querySelector(".people-name");
const positionElement = document.querySelector(".people-position");
const slider = document.querySelector(".slider-elements");
const arrows = document.querySelectorAll(".slider-arrow");

const getActivePerson = () =>
  document.querySelector(`.slider-elements img.active`);

function changeActivePerson(id) {
  const person = people.find((p) => p.id === (id || 1));
  photoElement.src = person.photoUrl;
  document
    .querySelector(`.slider-elements img[data-id="${id || 1}"]`)
    .classList.add("active");
  textElement.innerHTML = person.text;
  nameElement.innerHTML = person.name;
  positionElement.innerHTML = person.title;
}

function initSlider() {
  slider.innerHTML = people
    .map(
      (p) =>
        `<img
				class="${p.id === 1 ? "active" : ""}"
				src="${p.photoUrl}"
				alt=""
				data-id="${p.id}"
				>`
    )
    .join("");
}

initSlider();
changeActivePerson();

const persons = document.querySelectorAll(".slider-elements img");

Array.from(persons).forEach((p) => {
  p.addEventListener("click", (e) => {
    if (e.target.classList.contains("active")) {
      return;
    }
    persons.forEach((p) => p.classList.remove("active"));
    e.target.classList.add("active");
    changeActivePerson(+e.target.dataset.id);
  });
});

Array.from(arrows).forEach((a) => {
  a.addEventListener("click", (e) => {
    const activeElement = getActivePerson();
    const activeStep = +getActivePerson().dataset.id;
    activeElement.classList.remove("active");
    const step = +e.target.dataset.step;
    switch (true) {
      case activeStep + step === 0:
        changeActivePerson(people.length);
        return;
      case activeStep + step === people.length + 1:
        changeActivePerson(1);
      default:
        changeActivePerson(activeStep + step);
        return;
    }
  });
});
