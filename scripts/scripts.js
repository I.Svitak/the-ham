// Our Services
const navItems = document.querySelectorAll(".nav-item");
const navContent = document.querySelectorAll(".our-services-content li");
const activeClass = "active";

Array.from(navItems).forEach((element) => {
  element.addEventListener("click", ({ target }) => {
    const classList = target.classList;
    const itemIndex = target.dataset.tabindex;
    const content = Array.from(navContent).find(
      (li) => li.dataset.tabindex === itemIndex
    );

    if (Array.from(classList).includes(activeClass)) {
      return;
    }

    Array.from(navItems).forEach((element) => {
      element.classList.remove(activeClass);
    });

    Array.from(navContent).forEach((element) => {
      element.classList.remove(activeClass);
    });

    classList.add(activeClass);
    content.classList.add(activeClass);
  });
});
